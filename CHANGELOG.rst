^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package micros_swarm_framework
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.0.6 (2016-05-17)
-----------
* optimize the kernel code
* change the readme file

0.0.5 (2016-05-13)
-----------
* change the license to BSD
* perfect the package information
* optimize the kernel
* optimize code structure
* simplify synchronization protocol

0.0.4 (2016-05-11)
-----------
* repair the cmake bug

0.0.3 (2016-05-11)
-----------
* repair the headers bug

0.0.2 (2016-05-10)
------------------
* version 1.0
* Contributors: xuefengchang
